\contentsline {chapter}{\numberline {第1\ignorespaces 章\hspace {0.3em}}引\nobreakspace {}\nobreakspace {}言}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}研究背景}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}研究现状}{2}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}二维晶体生长}{2}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}二维准晶生长}{3}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}研究方法概述}{7}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}经典密度泛函理论}{7}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}分子动力学}{8}{subsection.1.3.2}
\contentsline {section}{\numberline {1.4}问题提出}{9}{section.1.4}
\contentsline {section}{\numberline {1.5}本文的研究内容的和内容框架}{10}{section.1.5}
\contentsline {chapter}{\numberline {第2\ignorespaces 章\hspace {0.3em}}相场晶体模型介绍}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}自由能泛函}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Cahn-Hilliard方程}{13}{section.2.2}
\contentsline {chapter}{\numberline {第3\ignorespaces 章\hspace {0.3em}}数值方法}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}相场晶体模型的拟谱方法}{14}{section.3.1}
\contentsline {section}{\numberline {3.2}半隐时间离散}{16}{section.3.2}
\contentsline {section}{\numberline {3.3}理论分析}{17}{section.3.3}
\contentsline {chapter}{\numberline {第4\ignorespaces 章\hspace {0.3em}}数值实验}{22}{chapter.4}
\contentsline {section}{\numberline {4.1}数值方法的有效性}{22}{section.4.1}
\contentsline {section}{\numberline {4.2}两尺度模型下晶体种子的两种生长模式}{23}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}第一种生长模式}{23}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}第二种生长模式}{23}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}两种子晶体准晶生长}{27}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}固定种子距离$d$}{27}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}旋转准晶种子}{27}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}旋转晶体种子}{28}{subsection.4.3.3}
\contentsline {subsubsection}{改变种子距离}{30}{section*.4}
\contentsline {section}{\numberline {4.4}准晶背景下的晶体生长}{33}{section.4.4}
\contentsline {subsubsection}{参数设置}{33}{section*.5}
\contentsline {subsubsection}{准晶背景下的晶体生长}{33}{section*.6}
\contentsline {chapter}{\numberline {第5\ignorespaces 章\hspace {0.3em}}总结与展望}{36}{chapter.5}
\contentsline {chapter}{致谢}{39}{chapter*.8}
